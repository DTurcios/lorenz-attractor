window.addEventListener("load", function () {
	"use strict";
	var w = 700, h = 700;
	var x = 0, y = 1 ,z = 2;

	//declare renderer variable
	var renderer = new THREE.WebGLRenderer();

	//Set size of renderer
	renderer.setSize(w, h);

	//Add the renderer to the html document body element
	document.getElementById("view").appendChild(renderer.domElement);
    
	//declare camera variable
	//assign three.js perspective camera to variable
	var near = 0.1, far = 1000000, fov = 50;
	var camera = new THREE.PerspectiveCamera(fov, w / h, near, far);  

	//set the position of the camera
	camera.position.set(0, 0, 100);
	var controls = new THREE.TrackballControls(camera, renderer.domElement);
    
	//declare and create the scene
	var scene = new THREE.Scene();
		scene.add(new THREE.AmbientLight(0x333333));

	//Makes axis and grid to easier navigate shape
	var axisHelper = new THREE.AxisHelper(50);
		scene.add(axisHelper);
	
	//Creates XZ Grid
	var size = 100000, divisions = 100;
	var gridXZ = new THREE.GridHelper(size, divisions);
		scene.add(gridXZ);
    
	//Create and declare line
	var obj = new THREE.Line(
        	new THREE.Geometry(),
		new THREE.LineBasicMaterial({color: 0xFF00FF}));//gives line a purple color
    
	//Add line to scene
	scene.add(obj);
    
	//lineAdd function definition
	var lineAdd = function (mesh, xs) {
		mesh.geometry.vertices.push(new THREE.Vector3(xs[x], xs[y], xs[z]));
		mesh.geometry.verticesNeedUpdate = true;
	};
    
	//Variables recieve their values
	var iSigma = document.getElementById("sigma");
	var iRho = document.getElementById("rho");
	var iBeta = document.getElementById("beta");

	//runge-kutta ODE: vector version
	var rk4 = function (dxsdt, x0s, t0, h) {
        	var time = t0 + h / 2, t1 = t0 + h;

        	var k1s = dxsdt(x0s, t0);//k1
        	var m1s = k1s.map(function (k1, i) {
		return x0s[i] + h * k1 / 2;});//function to find k2
        
		var k2s = dxsdt(m1s, time);//k2
        	var m2s = k2s.map(function (k2, i) {
		return x0s[i] + h * k2 / 2;});//function to find k3
        
		var k3s = dxsdt(m2s, time);//k3
        	var x1s = k3s.map(function (k3, i) {
		return x0s[i] + h * k3;});//function to find k4
        
		var k4s = dxsdt(x1s, t1);//k4
        	return x0s.map(function (x0, i) {
            	return x0 + h/6 * (k1s[i] + 2 * k2s[i] + 2 * k3s[i] + k4s[i]);
        	
	});
    	};
    
    	var xs = [1, 1, 1];//set (x,y,z) to (1,1,1) to start with
    		lineAdd(obj, xs);//line begins with line obj created and passing initial position
    	var h = 1/100; //time-step
    
    	//next function defined
    	var next = function () {
      	  	//Lorenz Attractor Equations
       	 	var sigma = iSigma.value, rho = iRho.value, beta = iBeta.value;
        	var dxsdt = function (xs, t) {
            	return [sigma * (xs[y] - xs[x]), 
                    xs[x] * (rho - xs[z]) - xs[y],
                    xs[x] * xs[y] - beta * xs[z]];

        };

        xs = rk4(dxsdt, xs, 0, h);//Next set of points are solved for using 
				// Lorenz Attractor Equations, current xyz, time, step
        
	lineAdd(obj, xs);
    	};

    	//Changing this variable changes refresh rate for line

    	var refresh = 1000;
    	for (var i = 0; i < refresh; i ++) next();
    
    	//loop function defined
    	var loop = function loop() {
        	requestAnimationFrame(loop);
        	obj.geometry.vertices.shift();
        	next();
        	controls.update();
        	renderer.clear();

     		//render the scene
        	renderer.render(scene, camera);
   	};
    	loop();
    	}, false);